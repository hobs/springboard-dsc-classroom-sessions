## Skills Checklist


- [x] getting help: ?, ??, [TAB], `dir()`, `vars()`, syntax-highlighting
- [ ] getting help: `help`, `print()`, `.__doc__`
- [ ] scalar numerical data types: `int`, `float`
- [ ] sequence container types: `str`, `bytes`, `list`, `tuple`
- [ ] sets: `set`, `.intersection`, `.__add__`, `.__subtract__`
- [ ] dicts: `dict`, `dict(enumerate(zip()))`, `dict.get()` `[k for k in dict()]`
- [x] `iter(c).next()`, `type(c)` vs `type(c[0])`
- [ ] mapping: `dict`, `Counter`

- [ ] reading Tracebacks and error messages
- [x] importing python packages: `this`, `os`, `sys`
- [x] style: var names (never reserved words/builtins!), zen, DRY, relative paths, urls 
- [x] `__builtins__`: `vars(__builtins__)`
- [x] classes: class, methods, attributes, `__init__`, `__dict__`, isattr, "everything is an object"
- [x] containers: `list, dict, set, len`
- [x] workflow: "reset & run all", side-effects, idempotence, persist your experiment log 
- [x] install & import python packages: `Pandas`, `tqdm`
- [x] `DataFrame`, `Series`,`array`
- [ ] conditional expressions: `if`, `else`, `>`, `<`, `==`, `!=`, `not`, `in` (vectorized)

- [x] python path manipulation: `pathlib`, this, os, sys, sys.path_append() 
- [x] navigate directories: `cd`, `pwd`, `ls -hal`, `/`, `~`, `*`, `$HOME`
- [ ] paths: PYTHONPATH, which python, `sys.path_append()`
- [ ] files: `with`, `open`, `write`
- [ ] manipulate files with bash: `!`, `mkdir ~/code`, `mv`, `cp`, `touch`, `!cat`
- [ ] files: `with`, `open`, `write`, `read`, `readlines`

- [ ] loops: `for`, `while`, `enumerate`
- [x] working with iterables: `zip`, `zip*`, `list()` coercion
- [ ] functions: `args`, `kwargs`, `def`, `return`
- [ ] scalar numerical data types: int, float
- [ ] sequence data types: str, bytes, list, tuple
- [ ] mapping: dict, Counter
- [ ] type coercion: `list(str)`, `dict([(1,2)]`, `dict(zip(range(4), 'abcd'))`

