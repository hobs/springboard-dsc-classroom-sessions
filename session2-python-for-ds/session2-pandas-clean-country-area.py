import pandas as pd

pd.options.display.max_columns = 1000
pd.options.display.max_rows = 5
dfs = pd.read_html("https://simple.m.wikipedia.org/wiki/List_of_countries_by_area")
dfs
len(dfs)
!pwd
!pip install lxml
#pip
pd.read_html("https://simple.m.wikipedia.org/wiki/List_of_countries_by_area")
dfs = _
for i, df in enumerate(dfs):
    print(f'df {i} shape: {df.shape}')
df = dfs[0]
df.head()
# It looks like it messed up the column headers. `Pos` and `Country` should be separate columns
# Rename the columns to use your naming convention so you can remember what's what
# Think ahead and be as explicit as possible about your names
# Use underscores if you like to use the attribute approach (dot syntax) to access columns rather than label lookup (square brackets)
df.columns = ['area_rank', 'name', 'area (km^2 & mile^2)']
df.head()
# let's see what type of data is in each column (dtype)
df.info()
# So we need to convert the rank number to an integer
# df['area_rank'].astype(int)
# a superstar tests their hypotheses
# int('-')
# int('–')
int(1_000)
# Notice anything different about the last line of the two error messages?
df['area (km^2 & mile^2)'] = df['area (km^2 & mile^2)'].str.rstrip(')')
df['area (km^2 & mile^2)'].dtype
df.head()
df['area'] = df['area (km^2 & mile^2)'].str.replace(',', '_')
df.head()
df['area'] = df.area.str.split('(')
df.head()
zip(*df['area'])
df
df1 = pd.DataFrame(list(zip(*df['area']))).T
df1
pd.concat([df, df1], axis=1).shape
df = pd.concat([df, df1], axis=1)
df.head()
columns = list(df.columns)
columns[-2:] = 'areakm areami'.split()
columns
# So that's why it didn't create integers when it first loaded the DataFrame
# Let's write a function to clean it up
def convert_type(x): # never use a builtin (such as `type`) as a variable name
    if x == '–':
        return None
    else:
        return int(x)
df.columns = columns
del df[0]
del df[1]
df.head()
df.columns = columns
# del df[0]
# del df[1]
df.head()
df['areakm'] = df['areakm'].astype(int)
df['areami'] = df['areami'].asteyp(int)
df['areakm'] = df['areakm'].astype(float)
df['areami'] = df['areami'].asteyp(float)
df['areakm'] = df['areakm'].astype(float)
df['areami'] = df['areami'].astype(float)
df['areakm'] = df['areakm'].astype(float)
df['areami'] = df['areami'].astype(float)
df.head()
df['areakm'] = df['areakm'].astype(float)
df['areami'] = df['areami'].astype(float)
df['areakm'] = df['areakm'].astype(int)
df['areami'] = df['areami'].astype(int)
df.head()
x = df.areakm
y = df.areami
df['name areakm areami'.split()].to_csv()
df['name areakm areami'.split()]
df['name areakm areami'.split()].to_csv('clean_country_area.csv')
df['name areakm areami'.split()].to_csv('clean_country_area.csv')
ls
df['name areakm areami'.split()].to_csv('clean_country_area.csv')
!ls
dfclean = pd.read_csv('clean_country_area.csv')
dfclean = pd.read_csv('clean_country_area.csv')
dfclean.head()
dfclean = pd.read_csv('clean_country_area.csv', index_col=0)
dfclean.head()
