
# Python Programming

* [Skill Checklist](skills-checklist.md)
* [Quizzes](quizzes.md)

### Resources

* https://realpython.com/quizzes/

* https://bogotobogo.com/python/python_interview_questions.php

* https://www.educative.io/courses/learn-python-3-from-scratch

* https://opensource.com/article/20/9/teach-python-jupyter

* https://gist.github.com/lheagy/f216db7220713329eb3fc1c2cd3c7826

* [Using jupyter notebooks for teaching](https://jupyter4edu.github.io/jupyter-edu-book/catalogue.html)
