Python Programming
importing python packages: this, os, sys, sys.path_append()
navigate directories: cd, pwd, ls -hal, /, ~, *, $HOME
finding, installing, importing python packages: sklearn, Pandas, tqdm
reading Tracebacks and error messages
conditional expressions: if, else, >, <, ==, !=, not, in
loops: for, while, enumerate, zip, zip*, tqdm
functions: args, kwargs, def, return
classes: class, methods, attributes, __init__, "everything is an object"
scalar numerical data types: int, float
sequence data types: str, bytes, list, tuple
mapping: dict, Counter
type coercion: list(str), dict([(1,2)], dict(zip(range(4), 'abcd'))
sets: set
getting help: help(), ?, ??, [TAB] completion, vars(), dir(), type(), print(), .__doc__)
files: with, open, write
manipulate files with bash: !, pwd, mkdir ~/code, mv, cp, touch
A docstrings is a tripplequoted string at the beginning of a function, class, or module (.py file) like this:
```
def fu
These are called doctests. The question is the python code the line prefixed with `>>>`. The correct answer is the value you would put on the line after the python code based on what that expression would output.
>>> dict(enumerate('Hello'))[1]
'E'

>>> dict(enumerate('Hello'))[:-1]
TypeError: unhashable type: 'slice'

>>> set('Oh Hello'.lower()) - set('Ohio ')
{'e', 'l'}

>>> dict(Counter(list('ABBA'))
Counter({'A': 2, 'B': 2})

>>> dict(zip(*['RD', '2'*2]))
{'R': '2', 'D': '2'}

>>> pwd
>>> set(Path('.').absolute().__str__()) - set(_)
set()

>>> def f(x):
...     while x > 2:
...         x -= 2
...     return x
>>> f(128)
2

>>> 'lower' in dir('ect')
True

>>> 'dir' in list('direct')
False

>>> [1, 2, 3] * 2
[1, 2, 3, 1, 2, 3]

>>> np.array([1, 2, 3]) * 2
[2, 4, 6]

>>> pd.Series([1, 2, 3]) - 1
0    0
1    1
2    2
dtype: int64

>>> pd.Series([1, 2, 3]) + pd.Series([3, 2, 1], index=range(1,4))
0    NaN
1    5.0
2    5.0
3    NaN

