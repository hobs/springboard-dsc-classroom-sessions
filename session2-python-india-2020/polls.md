**Poll 0: Session Files**

Q: Have you opened the folder the \'session2-python-for-data-science\'
file and opened the two notebooks it contains?

1.  Yes

2.  No

**Poll 1: Exercise - Pandas Warm Up**

Q. How are you doing on the exercise?

1.  Almost Done

2.  Need a bit more time

3.  Need more info/ hints

**Poll 2: Understanding the data set**

Q1. What is each row?

1.  A question in the survey

2.  An answer to a question in the survey

3.  **A participant\'s response to the survey**

4.  None of the above

Q2. What is each column?

1.  A question in the survey

2.  **The set of all answers to a given question**

3.  Both of the above

4.  None of the above

**Poll 3: Indexing**

Q: What is a multi-index?

1.  Indexing with a single value

2.  **Indexing with multiple values**

3.  Indexing with values in a hierarchy

4.  All of the above

**Poll 4: Troubleshooting**

Q: What went wrong?

1.  Incorrect syntax

2.  Missing values in the column

3.  Not all rows have two elements in the salary\_range column

4.  None of the above

**Poll 5: Feedback**

Q1 How did you like the session overall? (1 being the lowest and 5
highest.)

a.  1

b.  2

c.  3

d.  4

e.  5

Q2 How useful did you find the topics covered in the session?

a.  1

b.  2

c.  3

d.  4

e.  5

Q3 Do you feel confident in python and pandas now?

a.  1

b.  2

c.  3

d.  4

e.  5

Q4 How would you rate the instructor?

a.  1

b.  2

c.  3

d.  4

e.  5
