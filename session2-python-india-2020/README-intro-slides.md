# How are things?

<img src="img/Use this for Introduction - Python for data science0.png" width=500px />

<img src="img/Use this for Introduction - Python for data science1.png" width=500px />

<img src="img/Use this for Introduction - Python for data science2.png" width=500px />

# Python for Data Science

__Data Science Career Track: Online Classroom Session 2__

<span style="color:#48485E">Let’s put a face to the voice\.</span>

<span style="color:#48485E"> __Turn on your video__ </span>

# 

<img src="img/Use this for Introduction - Python for data science3.png" width=500px />

* <span style="color:#48485E">Remember the prize at stake \- an amazon gift card\!</span>
* <span style="color:#48485E">How we can interact?</span>
  * <span style="color:#48485E"> __Ongoing doubts:__ </span>
    * <span style="color:#48485E">Use chat</span>  __all ‘panellists and attendees’__
  * <span style="color:#48485E"> __Activities:__ </span>
    * <span style="color:#48485E">Put your answers on chat using all ‘panellists and attendees’</span>
    * <span style="color:#48485E">Unmute yourself and speak up\!</span>
    * <span style="color:#48485E">Keep yourself on mute when not speaking\.</span>

<img src="img/Use this for Introduction - Python for data science4.png" width=500px />

# Agenda

# Object Oriented programming (e.g. str, repr, len, .__str__, .__repr__, .__len__)
How to find what you’re looking for (help, ?, ??, [TAB]-completion, docs)
Directories, files, text files (head, tail, cat, more) 
Jupyter notebook (py files, ipynb files, !)
Interpreting error messages & tracebacks
Built in data types (numeric, text, sequences, binary)
Numpy data types (ndarrays)
Pandas data types (DataFrame, Series)
Loops and list comprehensions
Functions
Classes

<img src="img/Use this for Introduction - Python for data science5.png" width=500px />

# Are you ready?

# Poll Time!!

<img src="img/Use this for Introduction - Python for data science6.png" width=500px />

# Session Files

# This google drive folder contains a data science jobs survey dataset which we will explore in this session. This also has two Jupyter notebooks with the activities you will go through in the session.

Please download these files if you haven’t downloaded already..

Make sure that you edit the path within df = pd.read_csv('../data/multiple_choice_responses.csv') in the part 2 of the notebook named '01_pandas_intro' in case you use a different address for saving the data file. You can check by running df.shape. ]

<img src="img/Use this for Introduction - Python for data science7.png" width=500px />

