```python
import pandas as pd
import matplotlib.pyplot
%matplotlib inline
```

![](https://numfocus.org/wp-content/uploads/2016/07/pandas-logo-300.png)

# 1. Pandas Recap

## 1.1 Pandas lets you **read**, **process** and **analyze tabular data**.
## 1.2 Pandas DataFrames
Pandas DataFrames are a **labeled** two-dimensional data structure and is similar in spirit to an Excel worksheet or a relational database table with labeled rows and columns.Typically,
* There can be multiple rows and columns in the data.
* Each row represents a sample of data.
* Each column contains a different variable that describes the samples (rows).
* The data in every column is usually the same type of data – e.g. numbers, strings, dates.


The DataFrame can be created in many different ways such as 
1. Using another DataFrame.
2. Using a NumPy array or a composite of arrays that has a two-dimensional shape.
3. Using Pandas Series.
4. Produced from a file, such as a CSV file.
5. From a dictionary of one-dimensional structure such as lists.

## 1.3 Exercise - Pandas Warmup


```python
exam_data  = {
    'name': ['Anastasia', 'Dima', 'Katherine', 'James', 'Emily', 'Michael', 'Matthew', 'Laura', 'Kevin', 'Jonas'],
    'score': [12.5, 9, 16.5, 6, 9, 20, 14.5, 7.5, 8, 19],
    'attempts': [1, 3, 2, 3, 2, 3, 1, 1, 2, 1]
}
labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
df = pd.DataFrame(exam_data , index=labels)
df
```

### Solve the following steps:

1. Get the first 3 rows.
2. Get the columns 'name' and 'score'
3. Get the test score of Emily
4. Change the score in row 'd' to 11.5.
5. Select the rows where the number of attempts in the examination is greater than 2.
6. Select the rows the score is between 15 and 20 (inclusive).
7. Calculate the sum of the examination attempts by the students.
8. Calculate the average of the examination scores of the students.

#### Challenge
9. add a column 'quality' such that it is 'yes' if a student's score is 10 or above, and 'no' otherwise.


```python
# Q1 - Enter code here
```


```python
# Q2 - Enter code here
```


```python
# Q3 - Enter code here
```


```python
# Q4 - Enter code here
```


```python
# Q5 - Enter code here
```


```python
# Q6 - Enter code here
```


```python
# Q7 - Enter code here
```


```python
# Q8 - Enter code here
```


```python
# Q9 - Enter code here
```

# 2. Case Study: Kaggle's ML and DS Survey 2019


```python
df = pd.read_csv('../data/multiple_choice_responses.csv')
df.head()
```


```python
df.shape
```

## 2.1 Any initial thoughts?

---?

## 2.2 Understanding the dataset
* ### How do we interpret the dimensions of the data?
* ### What is each row?
    1. [ ] A question in the survey
    2. [ ] An answer to a question in the survey
    3. [ ] A participant's response to the survey
    4. [ ] None of the above
* ### What is each column?
    1. [ ] A question in the survey
    2. [ ] The set of all answers to a given question
    3. [ ] Both of the above
    4. [ ] None of the above


```python
# How do we get rows?
i = 0
df.loc[i]
```


```python
# How do we get a list of columns?
df.columns
```


```python
# How to select a particular column?
colname = 'Time from Start to Finish (seconds)'
df[colname]
```

## Question: How is this column useful?
### (Hint: Remember that we are looking at survey responses!)


```python
df[colname].min(), df[colname].max()
```

## Question: What went wrong?
### (Hint: Take a look at the first row)


```python
df.head()
```

### Checking the "type" of a column


```python
df[colname].dtype
```


```python
duration = df[colname].iloc[1:]
```


```python
duration.head()
```


```python
duration_in_ints = duration.apply(lambda x: int(x))
```


```python
duration_in_ints = duration.astype(int)
```


```python
%matplotlib inline
ax = duration_in_ints.hist(bins=50)
```

# Filtering data
## Boolean indices


```python
(duration_in_ints[duration_in_ints < 60]).hist()
```

## Filtering the data by column values


```python
to_drop = duration_in_ints.index[duration_in_ints < 60]
```


```python
to_drop
```


```python
df.drop(to_drop, axis=0, inplace=True)
```


```python
df.head()
```


```python
df.to_csv('../data/kaggle_clean.csv', index=False)
```


```python

```
