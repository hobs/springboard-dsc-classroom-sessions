```python
import pandas as pd
import matplotlib.pyplot as plt
%matplotlib inline
```


```python
sys
```


```python
df = pd.read_csv('../data/kaggle_clean.csv')
df.head()
```


    ---------------------------------------------------------------------------

    FileNotFoundError                         Traceback (most recent call last)

    <ipython-input-2-1c43ed4ffbcb> in <module>
    ----> 1 df = pd.read_csv('../data/kaggle_clean.csv')
          2 df.head()


    ~/.local/lib/python3.8/site-packages/pandas/io/parsers.py in read_csv(filepath_or_buffer, sep, delimiter, header, names, index_col, usecols, squeeze, prefix, mangle_dupe_cols, dtype, engine, converters, true_values, false_values, skipinitialspace, skiprows, skipfooter, nrows, na_values, keep_default_na, na_filter, verbose, skip_blank_lines, parse_dates, infer_datetime_format, keep_date_col, date_parser, dayfirst, cache_dates, iterator, chunksize, compression, thousands, decimal, lineterminator, quotechar, quoting, doublequote, escapechar, comment, encoding, dialect, error_bad_lines, warn_bad_lines, delim_whitespace, low_memory, memory_map, float_precision, storage_options)
        608     kwds.update(kwds_defaults)
        609 
    --> 610     return _read(filepath_or_buffer, kwds)
        611 
        612 


    ~/.local/lib/python3.8/site-packages/pandas/io/parsers.py in _read(filepath_or_buffer, kwds)
        460 
        461     # Create the parser.
    --> 462     parser = TextFileReader(filepath_or_buffer, **kwds)
        463 
        464     if chunksize or iterator:


    ~/.local/lib/python3.8/site-packages/pandas/io/parsers.py in __init__(self, f, engine, **kwds)
        817             self.options["has_index_names"] = kwds["has_index_names"]
        818 
    --> 819         self._engine = self._make_engine(self.engine)
        820 
        821     def close(self):


    ~/.local/lib/python3.8/site-packages/pandas/io/parsers.py in _make_engine(self, engine)
       1048             )
       1049         # error: Too many arguments for "ParserBase"
    -> 1050         return mapping[engine](self.f, **self.options)  # type: ignore[call-arg]
       1051 
       1052     def _failover_to_python(self):


    ~/.local/lib/python3.8/site-packages/pandas/io/parsers.py in __init__(self, src, **kwds)
       1865 
       1866         # open handles
    -> 1867         self._open_handles(src, kwds)
       1868         assert self.handles is not None
       1869         for key in ("storage_options", "encoding", "memory_map", "compression"):


    ~/.local/lib/python3.8/site-packages/pandas/io/parsers.py in _open_handles(self, src, kwds)
       1360         Let the readers open IOHanldes after they are done with their potential raises.
       1361         """
    -> 1362         self.handles = get_handle(
       1363             src,
       1364             "r",


    ~/.local/lib/python3.8/site-packages/pandas/io/common.py in get_handle(path_or_buf, mode, encoding, compression, memory_map, is_text, errors, storage_options)
        640                 errors = "replace"
        641             # Encoding
    --> 642             handle = open(
        643                 handle,
        644                 ioargs.mode,


    FileNotFoundError: [Errno 2] No such file or directory: '../data/kaggle_clean.csv'


## We have the same problem!


```python
pd.read_csv?
```


```python
df = pd.read_csv('../data/kaggle_clean.csv', header=[0, 1])
df.head()
```


```python
df.columns
```

## Question: What is a multi-index?

1. [ ] Indexing with a single value
2. [ ] Indexing with multiple values
3. [ ] Indexing with values in a hierarchy
4. [ ] All of the above


```python
df['Q1']
```


```python
df['Q1', 'What is your age (# years)?']
```

## Question: What is the difference between the above two outputs?

### Making it more manageable - keep the first level of indexing on the dataframe, keep the second only for refence


```python
# Note that Python has dictionary comprehensions too!
{k: v for k, v in df.columns}
```


```python
# An easier way:
dict(df.columns)
```

### Since we know which code corresponds to which question, we can get rid of the second level columns.


```python
# TODO: Add following steps:
# 3. When to use bar charts vs histograms?
# 4. When to use .value_counts?
# 5. Identifying metrics and dimensions. Simple groupmeans
```


```python
question_codes = dict(df.columns)
xdf = df.droplevel(1, axis=1)
```


```python
xdf.head()
```

### How do I find out what Q1 means?


```python
question_codes['Q1']
```


```python
# What is Q2?
question_codes['Q2']
```

### Exercise: How to find the gender ratio of this survey?


```python
# enter code here
```

## Looking at the Indian subset of the data


```python
question_codes['Q3']
```


```python
india = xdf[xdf['Q3'] == 'India']
india.head()
```

### Exploring salaries in India


```python
question_codes['Q10']
```


```python
india['Q10']
```


```python
# Counting null values
india['Q10'].isna().sum()
```


```python
# Dropping null values
india.dropna(subset=['Q10'], inplace=True)
india['Q10'].head()
```


```python
# How do we remove the $ symbol?
remove_dollar = lambda x: x.replace('$', '')
india['Q10'] = india['Q10'].apply(remove_dollar)
india['Q10'].head()
```

### Remove the commas with Pandas string functions


```python
india['Q10'] = india['Q10'].str.replace(',', '')
india['Q10'].head()
```


```python
# Getting min / max values from salary range
india['Q10'].str.split('-').head()
```


```python
india['salary_range'] = india['Q10'].str.split('-')
```


```python
# Getting min and max salaries from the range
```


```python
india['s_min'] = india['salary_range'].apply(lambda x: x[0])
india['s_max'] = india['salary_range'].apply(lambda x: x[1])
```

### Question: What went wrong?

1. [ ] Incorrect syntax
2. [ ] Missing values in the column
3. [ ] Not all rows have two elements in the `salary_range` column
4. [ ] None of the above


```python
# Find the length of each element in the 'salary_range' column
l = india['salary_range'].apply(len)
```


```python
l.min()
```


```python
india[l == 1].head()
```


```python
# modify the max value computation
india['s_max'] = india['salary_range'].apply(lambda x: x[1] if len(x) == 2 else x[0])
```


```python
india['s_min']
```


```python
# replace the '> 500000' with '500000'
```


```python
india['s_min'] = india['s_min'].str.replace('>', '')
```


```python
india['s_min'].astype(int).hist()
```


```python
ax = india['s_min'].astype(int).hist(bins=50)
ax.set_xlim(0, 100000)
```

## How to examine other non numerical columns?


```python
question_codes['Q5']
```


```python
india['Q5'].head()
```


```python
india['Q5'].value_counts()
```


```python
india['Q5'].value_counts().plot(kind='bar')
```


```python
india['Q5'].value_counts().plot(kind='barh')
```

### Exercise: Which job title has the highest average salary?
#### How do we break this down into smaller steps?


```python
# enter code here
```
