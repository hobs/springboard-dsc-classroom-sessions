#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[ ]:


sys


# In[2]:


df = pd.read_csv('../data/kaggle_clean.csv')
df.head()


# ## We have the same problem!

# In[ ]:


get_ipython().run_line_magic('pinfo', 'pd.read_csv')


# In[ ]:


df = pd.read_csv('../data/kaggle_clean.csv', header=[0, 1])
df.head()


# In[ ]:


df.columns


# ## Question: What is a multi-index?
# 
# 1. [ ] Indexing with a single value
# 2. [ ] Indexing with multiple values
# 3. [ ] Indexing with values in a hierarchy
# 4. [ ] All of the above

# In[ ]:


df['Q1']


# In[ ]:


df['Q1', 'What is your age (# years)?']


# ## Question: What is the difference between the above two outputs?

# ### Making it more manageable - keep the first level of indexing on the dataframe, keep the second only for refence

# In[ ]:


# Note that Python has dictionary comprehensions too!
{k: v for k, v in df.columns}


# In[ ]:


# An easier way:
dict(df.columns)


# ### Since we know which code corresponds to which question, we can get rid of the second level columns.

# In[ ]:


# TODO: Add following steps:
# 3. When to use bar charts vs histograms?
# 4. When to use .value_counts?
# 5. Identifying metrics and dimensions. Simple groupmeans


# In[ ]:


question_codes = dict(df.columns)
xdf = df.droplevel(1, axis=1)


# In[ ]:


xdf.head()


# ### How do I find out what Q1 means?

# In[ ]:


question_codes['Q1']


# In[ ]:


# What is Q2?
question_codes['Q2']


# ### Exercise: How to find the gender ratio of this survey?

# In[ ]:


# enter code here


# ## Looking at the Indian subset of the data

# In[ ]:


question_codes['Q3']


# In[ ]:


india = xdf[xdf['Q3'] == 'India']
india.head()


# ### Exploring salaries in India

# In[ ]:


question_codes['Q10']


# In[ ]:


india['Q10']


# In[ ]:


# Counting null values
india['Q10'].isna().sum()


# In[ ]:


# Dropping null values
india.dropna(subset=['Q10'], inplace=True)
india['Q10'].head()


# In[ ]:


# How do we remove the $ symbol?
remove_dollar = lambda x: x.replace('$', '')
india['Q10'] = india['Q10'].apply(remove_dollar)
india['Q10'].head()


# ### Remove the commas with Pandas string functions

# In[ ]:


india['Q10'] = india['Q10'].str.replace(',', '')
india['Q10'].head()


# In[ ]:


# Getting min / max values from salary range
india['Q10'].str.split('-').head()


# In[ ]:


india['salary_range'] = india['Q10'].str.split('-')


# In[ ]:


# Getting min and max salaries from the range


# In[ ]:


india['s_min'] = india['salary_range'].apply(lambda x: x[0])
india['s_max'] = india['salary_range'].apply(lambda x: x[1])


# ### Question: What went wrong?
# 
# 1. [ ] Incorrect syntax
# 2. [ ] Missing values in the column
# 3. [ ] Not all rows have two elements in the `salary_range` column
# 4. [ ] None of the above

# In[ ]:


# Find the length of each element in the 'salary_range' column
l = india['salary_range'].apply(len)


# In[ ]:


l.min()


# In[ ]:


india[l == 1].head()


# In[ ]:


# modify the max value computation
india['s_max'] = india['salary_range'].apply(lambda x: x[1] if len(x) == 2 else x[0])


# In[ ]:


india['s_min']


# In[ ]:


# replace the '> 500000' with '500000'


# In[ ]:


india['s_min'] = india['s_min'].str.replace('>', '')


# In[ ]:


india['s_min'].astype(int).hist()


# In[ ]:


ax = india['s_min'].astype(int).hist(bins=50)
ax.set_xlim(0, 100000)


# ## How to examine other non numerical columns?

# In[ ]:


question_codes['Q5']


# In[ ]:


india['Q5'].head()


# In[ ]:


india['Q5'].value_counts()


# In[ ]:


india['Q5'].value_counts().plot(kind='bar')


# In[ ]:


india['Q5'].value_counts().plot(kind='barh')


# ### Exercise: Which job title has the highest average salary?
# #### How do we break this down into smaller steps?

# In[ ]:


# enter code here

