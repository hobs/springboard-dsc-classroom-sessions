# Python Programming

## Poll 1

### Docstrings

A docstrings is a tripplequoted string at the beginning of a function, class, or module (.py file) like this:
```
def num_elements(container):
    """ Similar to `len`: Compute number of elements in any iterable """
    try:
        return len(container)
    except:
        return len(list(container))
```

These are called doctests. The question is the python code the line prefixed with `>>>`. The correct answer is the value you would put on the line after the python code based on what that expression would output.

>>> dict(enumerate('Hello'))[1]
'e'

>>> dict(enumerate('Hello'))[:-1]
TypeError: unhashable type: 'slice'

>>> set('Oh Hello'.lower()) - set('Ohio ')
{'e', 'l'}

>>> dict(Counter(list('ABBA'))
Counter({'A': 2, 'B': 2})

>>> dict(zip(*['RD', '2'*2]))
{'R': '2', 'D': '2'}

>>> pwd
>>> set(Path('.').absolute().__str__()) - set(_)
set()

>>> def f(x):
...     while x > 2:
...         x -= 2
...     return x
>>> f(128)
2

>>> 'lower' in dir('ect')
True

>>> 'dir' in list('direct')
False

>>> [1, 2, 3] * 2
[1, 2, 3, 1, 2, 3]

>>> np.array([1, 2, 3]) * 2
[2, 4, 6]

>>> pd.Series([1, 2, 3]) - 1
0    0
1    1
2    2
dtype: int64

>>> pd.Series([1, 2, 3]) + pd.Series([3, 2, 1], index=range(1,4))
0    NaN
1    5.0
2    5.0
3    NaN




