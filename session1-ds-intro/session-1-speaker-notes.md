## Passion for data

Don’t worry if you aren’t yet passionate about data.
Once you see how powerful it is at showing you things you never knew about the world, you will be converted.
Businesses learned to be data-driven because it helps them make money and grow.
You will too.


## BS Bingo Cartoon

Imagine you’re in a boardroom talking about how profit is declining with other business managers.
I’ve been in many of those meetings.
They are thoroughly unproductive.
Everybody has ideas.
But no one is willing to test their hypotheses and accept the results.
When humans make decisions together, politics is more important than facts.
As a data scientist you get to crash the BS bingo party.
You get to cut through all that BS with facts.
And everybody in the room will love you.
Because they don’t have to say anything that might make them look bad in the future.
You get to say “I don’t know”, but I can look at the data and find out, or we can come up with an experiment (app) for that.
Instead of describing and justifying and rationalizing why all the business decisions you make are correct, you get to provide people with measurable (SMART) predictions.
 And you get to have a conversation about how to improve them or what you can learn about them.
